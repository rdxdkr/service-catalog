use crate::{
    error::MyError,
    model::{NextGenService, RawTenant, Service, TenantMetrics, TenantMetricsWithFullName},
};
use axum::response::Result;
use axum::Json;
use axum_macros::debug_handler;
use serde_json::Value;
use std::{collections::HashMap, str::FromStr};

#[debug_handler]
pub async fn get_services() -> Result<Json<Vec<Service>>> {
    let mut tenants = get_raw_tenants().await?;
    let mut services_with_total_uses = HashMap::<String, u64>::new();

    for t in &mut tenants {
        let api_name = t.labels.metrics_path.replace("/metrics", "");
        let metrics_url = format!("{}{}{}", "https://", t.labels.job, t.labels.metrics_path);
        let services_url = format!(
            "{}{}{}{}",
            "https://", t.labels.job, api_name, "/api/services"
        );

        let raw_tenant_metrics = get_raw_tenant_metrics(&metrics_url).await?;
        let next_gen_services = get_next_gen_services(&services_url).await?;
        let tenant_metrics = raw_tenant_metrics
            .split('\n')
            .filter(|line| {
                let mut check = false;

                for ngs in &next_gen_services {
                    if line.contains(&ngs.slug_name) {
                        check = true;
                        break;
                    }
                }

                check && (line.contains(r#"status="7000""#) || line.contains(r#"status="9000""#))
            })
            .map(|line| TenantMetrics::from_str(line).unwrap())
            .collect::<Vec<_>>();
        let tenant_metrics_with_full_name = tenant_metrics
            .iter()
            .filter_map(|tm| {
                for ngs in &next_gen_services {
                    if ngs.slug_name == tm.service_slug_name {
                        return Some(TenantMetricsWithFullName {
                            service_full_name: ngs.full_name.clone(),
                            tenant_metrics: tm.clone(),
                        });
                    }
                }

                None
            })
            .collect::<Vec<_>>();

        for tm in tenant_metrics_with_full_name {
            *services_with_total_uses
                .entry(tm.service_full_name)
                .or_insert(0) += tm.tenant_metrics.uses;
        }
    }

    let mut result = services_with_total_uses
        .into_iter()
        .map(|pair| Service {
            service_full_name: pair.0,
            total_uses: pair.1,
        })
        .collect::<Vec<_>>();

    result.sort_by_key(|k| k.total_uses);
    Ok(Json(result))
}

async fn get_raw_tenants() -> Result<Vec<RawTenant>> {
    Ok(
        reqwest::get("https://www2.stanzadelcittadino.it/prometheus.json")
            .await
            .map_err(MyError::from)?
            .json::<Vec<RawTenant>>()
            .await
            .map_err(MyError::from)?,
    )
}

async fn get_raw_tenant_metrics(url: &String) -> Result<String> {
    Ok(reqwest::get(url)
        .await
        .map_err(MyError::from)?
        .text()
        .await
        .map_err(MyError::from)?)
}

async fn get_next_gen_services(url: &String) -> Result<Vec<NextGenService>> {
    let services = reqwest::get(url)
        .await
        .map_err(MyError::from)?
        .json::<Value>()
        .await
        .map_err(MyError::from)?;
    let services = services.as_array().unwrap();
    let mut result = Vec::<NextGenService>::new();

    for s in services.iter() {
        let s = s.as_object().unwrap();

        if s.get("flow_steps").is_some() {
            result.push(NextGenService {
                full_name: s["name"].as_str().unwrap().to_string(),
                slug_name: s["slug"].as_str().unwrap().to_string(),
            });
        }
    }

    Ok(result)
}
