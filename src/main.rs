mod error;
mod model;
mod services_handler;

use crate::services_handler::get_services;
use axum::{routing::get, Router};
use std::net::SocketAddr;
use tower::ServiceBuilder;
use tower_http::trace::TraceLayer;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

#[tokio::main]
async fn main() {
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(
            std::env::var("RUST_LOG")
                .unwrap_or_else(|_| "service_catalog=debug,tower_http=debug".into()),
        ))
        .with(tracing_subscriber::fmt::layer())
        .init();

    let app = Router::new()
        .route("/", get(greet))
        .route("/health_check", get(health_check))
        .route("/services", get(get_services))
        .layer(ServiceBuilder::new().layer(TraceLayer::new_for_http()));

    /*
     * l'indirizzo di ascolto è stato cambiato affinché il server possa funzionare correttamente in docker
     *
     * per un utilizzo in locale sarebbe meglio tenere 127.0.0.1, quindi si potrebbe scegliere l'indirizzo
     * adatto in base ad una variabile d'ambiente settata nel Dockerfile. in questo modo rimarrebbe 127.0.0.1
     * come default e verrebbe usato 0.0.0.0 solo durante l'esecuzione in un container o quando richiesto
     */
    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));

    tracing::debug!("listening on {}", addr);

    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn greet() -> &'static str {
    "Hello, World!"
}

async fn health_check() -> &'static str {
    "The server is running"
}
