use axum::response::IntoResponse;
use hyper::StatusCode;

pub struct MyError {
    pub error: reqwest::Error,
}

impl IntoResponse for MyError {
    fn into_response(self) -> axum::response::Response {
        (StatusCode::INTERNAL_SERVER_ERROR, self.error.to_string()).into_response()
    }
}

impl From<reqwest::Error> for MyError {
    fn from(e: reqwest::Error) -> Self {
        MyError { error: e }
    }
}
