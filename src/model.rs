use axum::Error;
use serde::{Deserialize, Serialize};
use std::str::FromStr;

#[derive(Serialize, Deserialize, Debug)]
pub struct RawTenant {
    pub targets: Vec<String>,
    pub labels: TenantLabels,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TenantLabels {
    pub job: String, // si potrebbe usare PathBuf
    pub env: Option<String>,
    #[serde(rename = "__scheme__")]
    pub scheme: String,
    #[serde(rename = "__metrics_path__")]
    pub metrics_path: String, // si potrebbe usare PathBuf
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TenantMetrics {
    pub service_slug_name: String,
    pub uses: u64,
}

impl FromStr for TenantMetrics {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut first_split = s.split(' ');
        let service_slug_name = first_split
            .next()
            .unwrap()
            .split(',')
            .nth(1) // avanza l'iteratore di 2 posizioni
            .unwrap()
            .split("service=")
            .filter(|s| !s.is_empty())
            .map(|s| s.replace('"', ""))
            .take(1)
            .collect();

        Ok(TenantMetrics {
            service_slug_name,
            uses: first_split
                .last()
                .unwrap()
                .to_string()
                .parse::<u64>()
                .unwrap(),
        })
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct NextGenService {
    pub full_name: String,
    pub slug_name: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TenantMetricsWithFullName {
    pub service_full_name: String,
    pub tenant_metrics: TenantMetrics,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Service {
    pub service_full_name: String,
    pub total_uses: u64,
}
