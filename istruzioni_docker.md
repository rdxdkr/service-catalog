Il `Dockerfile` è ispirato a quello che viene illustrato nel libro [Zero to Production in Rust](https://www.zero2prod.com/index.html?country=Italy&discount_code=VAT20) di Luca Palmieri. È scritto in modo da rendere il container risultante il più leggero possibile, utilizzando una versione minimale sia del sistema operativo che della *toolchain* di compilazione di Rust. Attualmente il container pesa 94.7MB.

Oltre a questo, nei passaggi di creazione del container è stato usato [cargo-chef](https://github.com/LukeMathWalker/cargo-chef) dello stesso Luca Palmieri per applicare facilmente il *layer caching* di Docker alle dipendenze del progetto, così da velocizzarne la compilazione in nuove versioni del container.

---

Per compilare: `docker build --tag service-catalog --file Dockerfile .`

Per eseguire: `docker run -p 3000:3000 service-catalog`